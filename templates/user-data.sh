#!/bin/bash

# ECS config
{
  echo "ECS_CLUSTER=${cluster_name}
        ECS_RESERVED_MEMORY=256"
} >> /etc/ecs/ecs.config

start ecs

echo "Done"